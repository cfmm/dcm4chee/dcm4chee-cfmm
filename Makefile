DCM4CHEE_VERSION:=2.18.3
PACKAGE:= dcm4chee-cfmm-$(VERSION)
DCM4CHEE:= $(abspath dcm4chee)-$(DCM4CHEE_VERSION)-mysql
JBOSS:= $(abspath jboss-4.2.3.GA)
LIBDIR:=$(CACHE)/lib

export DCM4CHEE
export JBOSS

all: package

package: download $(PACKAGE)
	$(MAKE) -C hsm install INSTALLDIR=../$(PACKAGE) LIBDIR=$(LIBDIR)
	$(MAKE) -C security install INSTALLDIR=../$(PACKAGE) LIBDIR=$(LIBDIR)
	$(MAKE) -C update install INSTALLDIR=../$(PACKAGE) LIBDIR=$(LIBDIR)
	zip -r $(PACKAGE).zip $(PACKAGE)

download:  dcm4chee-${DCM4CHEE_VERSION}-mysql jboss-4.2.3.GA
	@$(MAKE) -C hsm download INSTALLDIR=../$(PACKAGE) LIBDIR=$(LIBDIR)

$(CACHE)/dcm4chee-${DCM4CHEE_VERSION}-mysql.zip:
	wget -q -nc http://downloads.sourceforge.net/project/dcm4che/dcm4chee/${DCM4CHEE_VERSION}/dcm4chee-${DCM4CHEE_VERSION}-mysql.zip -O $@
	touch $@

$(CACHE)/jboss-4.2.3.GA-jdk6.zip:
	wget -q -nc http://downloads.sourceforge.net/project/jboss/JBoss/JBoss-4.2.3.GA/jboss-4.2.3.GA-jdk6.zip -O $@
	touch $@

jboss-4.2.3.GA: $(CACHE)/jboss-4.2.3.GA-jdk6.zip
	unzip -o -q $(CACHE)/jboss-4.2.3.GA-jdk6.zip
	touch $@

dcm4chee-${DCM4CHEE_VERSION}-mysql: $(CACHE)/dcm4chee-${DCM4CHEE_VERSION}-mysql.zip
	unzip -o -q $(CACHE)/dcm4chee-${DCM4CHEE_VERSION}-mysql.zip
	touch $@

$(PACKAGE):
	mkdir -p $(PACKAGE)

clean:
	$(RM) $(DEPENDENCE)
	$(RM) -r $(PACKAGE)
	$(RM) $(PACKAGE).zip

