package ca.robarts.cfmm.dcm4chee;

import java.util.List;

import java.io.IOException;
import java.io.StringWriter;

import javax.management.Attribute;
import javax.management.InstanceNotFoundException;
import javax.management.MBeanException;
import javax.management.MBeanServerConnection;
import javax.management.ObjectName;
import javax.management.ReflectionException;

import javax.naming.NamingException;
import javax.naming.InitialContext;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Element;

import org.xml.sax.SAXException;

import org.jboss.jmx.adaptor.rmi.RMIAdaptor;

import org.jboss.security.SecurityAssociation;
import org.jboss.security.SimplePrincipal;

import org.dcm4chex.archive.ejb.interfaces.AEDTO;

class AEUpdate
{
	private static final String DEFAULT_JNDI_NAME = "jmx/invoker/RMIAdaptor";
	private static MBeanServerConnection m_server;
	private static Document m_resultDocument;

	private MBeanServerConnection createConnection()
			throws NamingException
	{
		InitialContext ctx;

		ctx = new InitialContext();

        Object obj = ctx.lookup(DEFAULT_JNDI_NAME);
        ctx.close();

        if (!(obj instanceof RMIAdaptor))
        {
        	throw new ClassCastException("Object not of type: RMIAdaptorImpl, but: " + (obj == null ? "not found" : obj.getClass().getName()));
        }
        return (MBeanServerConnection) obj;
	}

	void init()
			throws NamingException, IOException, SAXException, ParserConfigurationException, TransformerException
	{

		// Read the input XML file
	    m_resultDocument = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(System.in);
    
	    Element docElement = m_resultDocument.getDocumentElement();
	    
	    // Set the access credentials
	    SecurityAssociation.setPrincipal(new SimplePrincipal(docElement.getAttribute("username")));
	    SecurityAssociation.setCredential(docElement.getAttribute("password"));
    	  
	    // Remove the security information
	    docElement.removeAttribute("password");
	    docElement.removeAttribute("username");
	    
	    m_server = this.createConnection();

        String service;
        String operation;
        String attribute;
        
        NodeList nList = docElement.getElementsByTagName("get");

		Element result;
		for (int i = 0; i < nList.getLength(); i++)
        {
        	result = (Element) nList.item(i);
        	
        	service = result.getAttribute("service");
        	attribute = result.getAttribute("attribute");
          
        	try
        	{
        		ObjectName name = new ObjectName(service);
        		String value = m_server.getAttribute(name, attribute).toString();
        		result.setAttribute("value", value);
        		result.setAttribute("status", "0");
        	}
        	catch (Exception e) {
        		result.setAttribute("status", "1");
            	e.printStackTrace();
        	}
        }

        nList = docElement.getElementsByTagName("set");
       
        for (int i = 0; i < nList.getLength(); i++)
        {
        	result = (Element) nList.item(i);

        	service = result.getAttribute("service");
        	attribute = result.getAttribute("attribute");
          
        	try
        	{
        		ObjectName name = new ObjectName(service);
        		Attribute attr = new Attribute(attribute, result.getAttribute("value"));
        		m_server.setAttribute(name, attr);
        		result.setAttribute("status", "0");
        	}
        	catch (Exception e) {
        		result.setAttribute("status", "1");
            	e.printStackTrace();
        	}
        }
       
        nList = docElement.getElementsByTagName("invoke");
       
        for (int i = 0; i < nList.getLength(); i++)
        {
        	result = (Element) nList.item(i);

        	service = result.getAttribute("service");
        	operation = result.getAttribute("operation");
          
			try
			{
				Element eResult = m_resultDocument.createElement("returns");
				ObjectName name = new ObjectName(service);
            	Object oResult = invoke(name, operation, result.getElementsByTagName("argument"));
				parseResult(oResult, operation, eResult);
				result.appendChild(eResult);
				result.setAttribute("status", "0");
			}
			catch (Exception e)
			{
				result.setAttribute("status", "1");
				e.printStackTrace();
			}
        }
       
        printDocument(m_resultDocument);
	}
   
	private static void printDocument(Document doc)
			throws TransformerException
	{
		TransformerFactory tf = TransformerFactory.newInstance();
	    Transformer transformer = tf.newTransformer();
	    transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "no");
	    transformer.setOutputProperty(OutputKeys.METHOD, "xml");
	    transformer.setOutputProperty(OutputKeys.INDENT, "yes");
	    transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
	    transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");

	    StringWriter writer = new StringWriter();
	    
	    transformer.transform(new DOMSource(doc), 
	         new StreamResult(writer));

	    System.out.println(writer.toString());
	}
   
	private void parseAEs(Object data, Element result)
	{
		List<?> aeTitles = (List<?>) data;

         
		for (Object obj : aeTitles)
		{
			parseAE(obj, result);
		}
	}
	
	private void parseAE(Object data, Element result)
	{
		AEDTO aet = (AEDTO) data;
		String value;

		Element ae = m_resultDocument.createElement("server");
	  
		ae.setAttribute("pk", String.valueOf(aet.getPk()));
		ae.setAttribute("aet", aet.getTitle());
		ae.setAttribute("host", aet.getHostName());
		ae.setAttribute("port", String.valueOf(aet.getPort()));

		value = aet.getCipherSuitesAsString();
		if (value == null)
			value = "";
		ae.setAttribute("cipher", value);

		value = aet.getIssuerOfPatientID();
		if (value == null)
			value = "";
		ae.setAttribute("pat_id_issuer", value);

		value = aet.getIssuerOfAccessionNumberAsString();
		if (value == null)
			value = "";
		ae.setAttribute("acc_no_issuer", value);

		value = aet.getUserID();
		if (value == null)
			value = "";
		ae.setAttribute("user_id", value);

		value = aet.getPassword();
		if (value == null)
			value = "";
		ae.setAttribute("passwd", value);
		
		value = aet.getFileSystemGroupID();
		if (value == null)
			value = "";
		ae.setAttribute("fs_group_id", value);

		value = aet.getGroup();
		if (value == null)
			value = "";
		ae.setAttribute("ae_group", value);

		value = aet.getDescription();
		if (value == null)
			value = "";
		ae.setAttribute("description", value);

		value = aet.getStationName();
		if (value == null)
			value = "";
		ae.setAttribute("station_name", value);
		
		value = aet.getInstitution();
		if (value == null)
			value = "";
		ae.setAttribute("institution", value);

		value = aet.getDepartment();
		if (value == null)
			value = "";
		ae.setAttribute("department", value);

		value = aet.getWadoURL();
		if (value == null)
			value = "";
		ae.setAttribute("wado_url", value);

		ae.setAttribute("installed", String.valueOf(aet.isInstalled()));

		result.appendChild(ae);
	}

	private void parseResult(Object data, String operation, Element result)
	{
		if (data == null)
			return;
		
		if (operation.equals("listAEs"))
		{
			parseAEs(data, result);
		}
		else if (operation.equals("getAE"))
		{
			parseAE(data, result);
		}
		else 
		{
			// default assume data is a string
			result.setAttribute("value", data.toString());
		}
	}
	
	private Object invoke(ObjectName name, String operation, NodeList data) throws InstanceNotFoundException, MBeanException, ReflectionException, IOException
	{
		Object [] params = null;
		String [] signature = null;
		
		int lParams = data.getLength();
		
		if (lParams > 0)
		{
			params = new Object[lParams];
			signature = new String[lParams];
		
			// Set the signature to all null values to enable checking for repeating
			// order values
			for (int j = 0; j < lParams; j++)
			{
				signature[j] = null;
			}
			
			for (int j = 0; j < lParams; j++)
			{
				Element eParameter = (Element) data.item(j);
				
				int index = Integer.parseInt(eParameter.getAttribute("order"));
				
				if ((index >= lParams) || (index < 0))
					throw new IndexOutOfBoundsException();
				
				// Check if it was set once already
				if (signature[index] != null)
					throw new IndexOutOfBoundsException();
				
				String type = eParameter.getAttribute("type");
				
				if (type.equalsIgnoreCase("long"))
				{
					signature[index] = long.class.getName();
					params[index] = Long.parseLong(eParameter.getTextContent());
				}
				else if (type.equalsIgnoreCase("integer"))
				{
					signature[index] = int.class.getName();
					params[index] = Integer.parseInt(eParameter.getTextContent());
				}
				else if (type.equalsIgnoreCase("boolean"))
				{
					signature[index] = boolean.class.getName();
					params[index] = Boolean.parseBoolean(eParameter.getTextContent());
				}
				else //if (type.equalsIgnoreCase("string"))
				{
					// Anything with unspecified type is assumed to be a string
					signature[index] = String.class.getName();
					params[index] = eParameter.getTextContent();
				}
			}
		}

		// At this point all parameters have been set since lParams where processed with
		// no repetitions and all between [0, lParams)
		return m_server.invoke(name, operation, params, signature);
		
	}
}
