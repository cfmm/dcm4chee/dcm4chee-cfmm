#!/bin/bash
PROGNAME=`basename $0`
DIRNAME=`dirname $0`

JBOSS_HOME="/opt/dcm4chee"
export JBOSS_HOME

# Setup the classpath
JBOSS_CLASSPATH="$JBOSS_HOME/client/jbossall-client.jar"
JBOSS_CLASSPATH="$JBOSS_CLASSPATH:$JBOSS_HOME/server/default/lib/dcm4chee-ejb-client.jar"
JBOSS_CLASSPATH="$JBOSS_CLASSPATH:$DIRNAME/cfmmUpdate.jar"

# Setup the java endorsed dirs
JBOSS_ENDORSED_DIRS="$JBOSS_HOME/lib/endorsed"

exec java \
    $JAVA_OPTS \
    -Djava.endorsed.dirs="$JBOSS_ENDORSED_DIRS" \
    -Dprogram.name="$PROGNAME" \
    -classpath $JBOSS_CLASSPATH \
    ca.robarts.cfmm.dcm4chee.AE "$@"
