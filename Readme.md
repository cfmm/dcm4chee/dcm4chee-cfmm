Java package for customizing dcm4chee

Prior to using the IntelliJ IDE run the command

```
mkdir cache; make CACHE=`pwd`/cache download
```

This will download all the libraries required by the application for compiling and testing.

java
====

hsm
---

Java HSM module for archiving data to B2 Cloud Storage.

The `b2cloud.jar` file must be installed into `/opt/dcm4chee`.

security
--------

Java package with a JBoss Login Module that uses Start TLS for communication 
with LDAP server. It is based on LdapExtLoginModule but exteneded to use Start 
TLS.

The `cfmmLogin.jar` file must be placed in `[DCM4CHEE_BASE]/server/default/deploy`


update
------

Shell script to call a Java program that interacts with dcm4chee server to edit 
properties through the JMX Management system.

The `cfmmUpdate.jar` file needs to be installed with the `ae.sh` script.

Avaiable commands:

1. update
   Reads XML data from stdin that defines an Application Entity Titles.
1. clear
   Clears the cache of all XSL templates.
1. permissions
   Write the file name of the permissions XSL file to stdout.
1. coerce
   Write the directory for the XSL files defining coercions to stdout. The 
   cfindrq.xsl or cfindrsp.xsl files are in this directory or in a 
   sub-directory with an AE Title name maching from/to QR SCU.
1. forwards
   Write the directory for the XSL files defining fowards to stdout. The 
   forwards.xsl file is in this directory, or in a sub-directory with an AE 
   Title name maching Store SCU.
1. list (or nothing)
   Print a list of existing AE Titles in XML to stdout.

The first two lines of stdin must always be the username and password, 
respectively, which should be used to connect to the JMX Management system.
