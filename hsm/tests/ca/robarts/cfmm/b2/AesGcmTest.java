package ca.robarts.cfmm.b2;

import org.bouncycastle.crypto.InvalidCipherTextException;
import org.bouncycastle.crypto.io.InvalidCipherTextIOException;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

import static org.junit.Assert.*;

/**
 * Created by mklassen on 2017-02-03.
 *
 * Testing for online encryption/decryption
 */
public class AesGcmTest {

    private static final String password = "password";
    private static final String aad = "Additional Validation Text";
    private static final String input = "This is the plain text message to encode";

    @Test
    public void crypt() throws Exception {
        InputStream is = new ByteArrayInputStream(input.getBytes());
        ByteArrayOutputStream os = new ByteArrayOutputStream();

        String digest = AesGcm.encrypt(password, aad, is, os);

        InputStream is2 = new ByteArrayInputStream(os.toByteArray());
        ByteArrayOutputStream os2 = new ByteArrayOutputStream();

        String digest2 = AesGcm.decrypt(password,aad, is2, os2);

        assertNotEquals(input, os.toString());
        assertEquals(digest, digest2);
        assertEquals(input, os2.toString());
    }

    @Test
    public void nullPassword() throws Exception {
        InputStream is = new ByteArrayInputStream(input.getBytes());
        ByteArrayOutputStream os = new ByteArrayOutputStream();

        boolean thrown = false;
        try {
            AesGcm.encrypt("", aad, is, os);
        } catch (IOException e) {
            thrown = true;
        }
        assertTrue(thrown);
    }

    @Test
    public void nullAad() throws Exception {
        InputStream is = new ByteArrayInputStream(input.getBytes());
        ByteArrayOutputStream os = new ByteArrayOutputStream();

        boolean thrown = false;
        try {
            AesGcm.encrypt(password, "", is, os);
        } catch (IOException e) {
            thrown = true;
        }
        assertTrue(thrown);
    }

    @Test
    public void password() throws Exception {
        InputStream is = new ByteArrayInputStream(input.getBytes());
        ByteArrayOutputStream os = new ByteArrayOutputStream();

        AesGcm.encrypt(password, aad, is, os);

        assertNotEquals(input, os.toString());

        InputStream is2 = new ByteArrayInputStream(os.toByteArray());
        ByteArrayOutputStream os2 = new ByteArrayOutputStream();

        boolean thrown = false;
        try {
            AesGcm.decrypt("wrong_password", aad, is2, os2);
        }
        catch (InvalidCipherTextIOException e) {
            thrown = true;
        }
        assertTrue(thrown);
    }

    @Test
    public void aad() throws Exception {
        InputStream is = new ByteArrayInputStream(input.getBytes());
        ByteArrayOutputStream os = new ByteArrayOutputStream();

        AesGcm.encrypt(password, aad, is, os);

        assertNotEquals(input, os.toString());

        InputStream is2 = new ByteArrayInputStream(os.toByteArray());
        ByteArrayOutputStream os2 = new ByteArrayOutputStream();

        boolean thrown = false;
        try {
            AesGcm.decrypt(password, "wrong_aad", is2, os2);
        }
        catch (InvalidCipherTextIOException e) {
            thrown = true;
        }
        assertTrue(thrown);
    }

    @Test
    public void byteChange() throws Exception {
        InputStream is = new ByteArrayInputStream(input.getBytes());
        ByteArrayOutputStream os = new ByteArrayOutputStream();

        AesGcm.encrypt(password, aad, is, os);

        assertNotEquals(input, os.toString());

        byte [] data = os.toByteArray();
        data[0] += 1;
        InputStream is2 = new ByteArrayInputStream(data);
        ByteArrayOutputStream os2 = new ByteArrayOutputStream();

        boolean thrown = false;
        try {
            AesGcm.decrypt(password, aad, is2, os2);
        }
        catch (InvalidCipherTextIOException e) {
            thrown = true;
        }
        assertTrue(thrown);
    }
}