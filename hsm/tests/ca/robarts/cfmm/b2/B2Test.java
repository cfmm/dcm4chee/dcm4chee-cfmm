package ca.robarts.cfmm.b2;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.FileWriter;

import static org.junit.Assert.*;

/**
 * Created by mklassen on 2017-02-03.
 *
 * Testing for B2 cloud storage
 */
public class B2Test {
    private final static String bucketId = "0123456789abcdef012345678";
    private final static String accountId = "";
    private final static String applicationKey = "";
    private final static String password = "ji2vwAgZaMu2q7FbwFpZaXAQ4cqBUdy3i9F9hL3bdeup2VjDp4";
    private final static String aad = "t2uY2QprTKZCQ3fpwLmPqwPzZn6h7UHsFu62fcFgN9HUP7XV3q";
    private final static long partSize = 100000000L;

    private B2 client;

    @Rule
    public TemporaryFolder data = new TemporaryFolder();

    @Before
    public void setup() {
        client = new B2();
        client.setBucketId(bucketId);
        client.setAccountId(accountId);
        client.setApplicationKey(applicationKey);
        client.setPassword(password);
        client.setAad(aad);
        client.setPartSize(partSize);
    }

    @Test
    public void getBucketId() throws Exception {
        client.setBucketId(bucketId);
        String result = client.getBucketId();
        assertEquals(bucketId, result);
    }

    @Test
    public void getAccountId() throws Exception {
        client.setAccountId(accountId);
        String result = client.getAccountId();
        assertEquals(accountId, result);
    }

    @Test
    public void getApplicationKey() throws Exception {
        client.setAccountId(applicationKey);
        String result = client.getAccountId();
        assertEquals(applicationKey, result);
    }

    @Test
    public void getPassword() throws Exception {
        client.setAccountId(password);
        String result = client.getAccountId();
        assertEquals(password, result);
    }

    @Test
    public void getAuthentication() throws Exception {
        client.setAccountId(aad);
        String result = client.getAccountId();
        assertEquals(aad, result);
    }

    @Test
    public void getPartSize() throws Exception {
        client.setPartSize(partSize);
        long result = client.getPartSize();
        assertEquals(partSize, result);
    }

    @Test
    public void communication() throws Exception {

        File tmpFile1 = data.newFile();
        File tmpFile2 = data.newFile();

        FileWriter wr = new FileWriter(tmpFile1);
        for (int i=0; i < 1000; i++) {
            wr.write(new Integer(i).toString() + "\n");
        }
        wr.close();

        String fileId = client.upload(tmpFile1, "b2-test");

        client.download(tmpFile2, fileId);

        assertTrue(FileUtils.contentEquals(tmpFile1, tmpFile2));

        assertTrue(client.verify(fileId));
    }

}