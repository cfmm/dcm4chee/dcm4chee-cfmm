package ca.robarts.cfmm.b2;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigInteger;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.util.Arrays;
import java.util.Random;

import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;

import org.bouncycastle.crypto.digests.SHA1Digest;
import org.bouncycastle.crypto.engines.AESEngine;
import org.bouncycastle.crypto.io.*;
import org.bouncycastle.crypto.modes.AEADBlockCipher;
import org.bouncycastle.crypto.modes.GCMBlockCipher;
import org.bouncycastle.crypto.params.AEADParameters;
import org.bouncycastle.crypto.params.KeyParameter;
import org.bouncycastle.util.io.TeeOutputStream;


class AesGcm {
	
	// Encrypt/Decrypt functions for AES-GCM (No padding)

	private static final String KEYGEN_SPEC = "PBKDF2WithHmacSHA1";
	private static final int SALT_LENGTH = 16;
	private static final int KEY_LENGTH = 32;
	private static final int IV_LENGTH = 12;
	private static final int TAG_LENGTH = 16;
	private static final int KEY_ITERATIONS = 100000;
	private static final int BUFFER_SIZE = 16384;
	
	
	private static byte[] generateSalt()
	{
		Random r = new SecureRandom();
		byte[] salt = new byte[SALT_LENGTH];
		r.nextBytes(salt);
		return salt;
	}

	private static Keys keygen(char[] password, byte[] salt) throws IOException
	{
		SecretKeyFactory factory;
		try
		{
			factory = SecretKeyFactory.getInstance(KEYGEN_SPEC);
		}
		catch (NoSuchAlgorithmException err) {
			throw new IOException("Invalid KEYGEN_SPEC");
		}

		// Lengths are specified in bytes, but keys are specified in bits
		KeySpec spec = new PBEKeySpec(password, salt, KEY_ITERATIONS, (KEY_LENGTH + IV_LENGTH) * 8);
		SecretKey tmp;
		try 
		{
			tmp = factory.generateSecret(spec);
		} 
		catch (InvalidKeySpecException err) {
			throw new IOException("Invalid Key Specification");
		}
		
		// Split into the key and iv (nonce)
		byte[] fullKey = tmp.getEncoded();
		KeyParameter key = new KeyParameter(Arrays.copyOfRange(fullKey, 0, KEY_LENGTH));
		byte[] iv = Arrays.copyOfRange(fullKey, KEY_LENGTH, fullKey.length);
		return new Keys(key, iv);
	}
	
	private static class Keys
	{
		final KeyParameter key;
		final byte[] iv;
		Keys(KeyParameter key, byte[] iv)
		{
			this.key = key;
			this.iv = iv;
		}
	}
	
	static String encrypt(String password, String aad, InputStream is, OutputStream os)
			throws IOException
	{
        if (aad.length() < 1) {
            throw new IOException("Invalid additional authentication data");
        }

		byte[] salt = generateSalt();

		Keys keys = keygen(password.toCharArray(), salt);
		
		AEADBlockCipher cipher = new GCMBlockCipher( new AESEngine());
		AEADParameters params = new AEADParameters(keys.key, TAG_LENGTH * 8, keys.iv, aad.getBytes());
		cipher.init(true, params);

		DigestOutputStream digest = new DigestOutputStream(new SHA1Digest());

		TeeOutputStream tee = new TeeOutputStream(os, digest);
		
		tee.write(salt);
		
		CipherOutputStream cipherOS = new CipherOutputStream(tee, cipher);
		
		int n;
		byte[] buffer = new byte[BUFFER_SIZE];
		while((n = is.read(buffer)) > -1)
		{
			cipherOS.write(buffer,0,n);
		}
		cipherOS.close();
		digest.close();
		tee.close();
		
		return String.format("%040x",  new BigInteger(1, digest.getDigest()));
	}
	
	static String decrypt(String password, String aad, InputStream is, OutputStream os)
			throws IOException
	{
        if (aad.length() < 1) {
            throw new IOException("Invalid additional authentication data");
        }

		DigestInputStream digest = new DigestInputStream(is, new SHA1Digest());
		
		byte[] salt = new byte[SALT_LENGTH];
		
		int n = digest.read(salt, 0, SALT_LENGTH);
		if (n != SALT_LENGTH)
			throw new IOException("Failed to read salt");
		
		Keys keys = keygen(password.toCharArray(), salt);
		
		AEADBlockCipher cipher = new GCMBlockCipher( new AESEngine() );
		AEADParameters params = new AEADParameters(keys.key, TAG_LENGTH * 8, keys.iv, aad.getBytes());
		cipher.init(false, params);
		
		CipherInputStream cipherOS = new CipherInputStream(digest, cipher);
		byte[] buffer = new byte[BUFFER_SIZE];
        while ((n = cipherOS.read(buffer)) > -1) {
            os.write(buffer, 0, n);
        }
		cipherOS.close();
		n = digest.getDigest().getDigestSize();
		buffer = new byte[n];
		digest.getDigest().doFinal(buffer, 0);
		digest.close();
		return String.format("%040x",  new BigInteger(1, buffer));
	}
	
}
