package ca.robarts.cfmm.b2;

import java.io.File;
import java.io.IOException;

import org.dcm4chex.archive.common.FileStatus;
import org.dcm4chex.archive.hsm.module.AbstractHSMModule;
import org.dcm4chex.archive.hsm.module.HSMException;
import org.dcm4chex.archive.util.FileUtils;

public class B2HSMModule extends AbstractHSMModule {

    private final B2 client = new B2();

    private File outgoingDir;
    private File absOutgoingDir;

    private File incomingDir;
    private File absIncomingDir;

    public final String getOutgoingDir() {
        return outgoingDir.getPath();
    }

    public final void setOutgoingDir(String dir) {
        this.outgoingDir = new File(dir);
        this.absOutgoingDir = FileUtils.resolve(this.outgoingDir);
    }

    public final String getIncomingDir() {
        return incomingDir.getPath();
    }

    public final void setIncomingDir(String dir) {
        this.incomingDir = new File(dir);
        this.absIncomingDir = FileUtils.resolve(this.incomingDir);
    }

    // Most parameters are passed through to the B2 client for management
    public final String getBucketId() {
        return client.getBucketId();
    }

    public final void setBucketId(String bucketId) {
        client.setBucketId(bucketId);
    }

	public String getAccountId() {
		return client.getAccountId();
	}

	public void setAccountId(String accountId) {
        client.setAccountId(accountId);
	}

	public String getApplicationKey() {
		return client.getApplicationKey();
	}

	public void setApplicationKey(String applicationKey) {
		client.setApplicationKey(applicationKey);
	}

	public String getPassword() {
        return client.getPassword();
	}

	public void setPassword(String password) {
        client.setPassword(password);
	}

	public String getAuthentication() {
        return client.getAad();
	}

	public void setAuthentication(String aad) {
        client.setAad(aad);
	}

	public long getMaximumRetries() {
        return client.getMaximumRetries();
    }

    public void setMaximumRetries(long maximumRetires) {
        client.setMaximumRetries(maximumRetires);
    }

    public long getLargeFileLimit() {
        return client.getLargeFileLimit();
    }

    public void setLargeFileLimit(long largeFileLimit) {
        client.setLargeFileLimit(largeFileLimit);
    }

    public long getMaximumDelay() {
        return client.getMaximumDelay();
    }

    public void setMaximumDelay(long maximumDelay) {
        client.setMaximumDelay(maximumDelay);
    }

    public long getPartSize() {
        return client.getPartSize();
    }

    public void setPartSize(long partSize) {
        client.setPartSize(partSize);
    }
    // End of B2 managed parameters

    private void deleteFile(File file){
        // Cleanup the file
        if (!file.delete()) {
            log.error("Failed to delete " + file);
        }
        else{
            log.debug("Successfully deleted " + file);
        }
    }

    @Override
    public void failedHSMFile(File file, String fsID, String filePath)
            throws HSMException {
        log.debug("failedHSMFile called with file=" + file + ", fsID=" + fsID
                + ", filePath=" + filePath);

        // FileCopy failed
        this.deleteFile(file);
    }

    @Override
    public File fetchHSMFile(String fsID, String filePath) throws HSMException {
        log.debug("fetchHSMFile called with fsID=" + fsID + ", filePath="
                + filePath);

        // filePath should hold the B2 fileId

        if (absIncomingDir.mkdirs()) {
            log.info("M-WRITE " + absIncomingDir);
        }

        File tarFile;
        try {
            tarFile = File.createTempFile("b2_", ".tar", absIncomingDir);
        } catch (IOException x) {
            throw new HSMException("Failed to create temp file in "
                    + absIncomingDir, x);
        }

        try {
        	client.download(tarFile, filePath);
            log.info("Fetched bucketId=" + client.getBucketId()  + ", fileId=" + filePath);
        } catch (Exception x) {
            log.error("Failed to download bucketId=" + client.getBucketId()  + ", filedId=" + filePath);
            throw new HSMException("Failed to retrieve " + filePath, x);
        }
        return tarFile;
    }

    @Override
    public void fetchHSMFileFinished(String fsID, String filePath, File file)
            throws HSMException {
        log.debug("fetchHSMFileFinished called with fsID=" + fsID
                + ", filePath=" + filePath + ", file=" + file);
        log.info("M-DELETE " + file);
        if (!file.delete()) {
            log.error("Failed to delete file " + file);
        }
    }

    @Override
    public File prepareHSMFile(String fsID, String filePath)
            throws HSMException {
        log.debug("prepareHSMFile called with fsID=" + fsID + ", filePath="
                + filePath);
        return new File(absOutgoingDir, new File(filePath).getName());
    }

    @Override
    public Integer queryStatus(String fsID, String filePath, String userInfo)
            throws HSMException {
        log.debug("queryStatus called with fsID=" + fsID + ", filePath="
                + filePath + ", userInfo=" + userInfo);

        // filePath should hold the B2 fileId

        try {
        	if (client.verify(filePath)) {
                log.debug(filePath + " exists");
                return FileStatus.ARCHIVED;
            }
        	else {
                log.debug(filePath + " does not exist");
            }
        } catch (Exception x) {
            log.error("Failed to verify bucketId=" + client.getBucketId() + ", fileId=" + filePath);
            throw new HSMException("Could not validate: " + filePath, x);
        }

        return FileStatus.DEFAULT;
    }

    @Override
    public String storeHSMFile(File file, String fsID, String filePath)
            throws HSMException {
        log.debug("storeHSMFile called with file=" + file + ", fsID=" + fsID
                + ", filePath=" + filePath);
        try {
        	String fileId = client.upload(file, filePath);
            log.info("Uploaded " + file + "to bucketId=" + client.getBucketId()  + ", fileId=" + filePath +", fileId=" + fileId);
            this.deleteFile(file);
            return fileId;
        } catch (Exception x) {
            log.error("Failed to upload " + file + " to bucketId=" + client.getBucketId()  + ", fileId=" + filePath);
            throw new HSMException("Could not store: " + filePath, x);
        }
    }
}
