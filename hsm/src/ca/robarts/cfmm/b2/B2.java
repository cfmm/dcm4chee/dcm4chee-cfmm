package ca.robarts.cfmm.b2;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.zip.DeflaterInputStream;
import java.util.zip.InflaterOutputStream;

import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.entity.FileEntity;

import org.json.JSONObject;
import synapticloop.b2.B2ApiClient;
import synapticloop.b2.exception.B2ApiException;
import synapticloop.b2.response.*;

public class B2 {

    private static final String MIME_TYPE = "application/octet-stream";
	private static final long LARGE_FILE_LIMIT = 5000000000L;

	private String accountId;
	private String applicationKey;
	private String password;
	private String aad;
    private String bucketId;

	private long largeFileLimit = 5000000000L;
	private long partSize = 500000000L;
	private long recommendedPartSize = 100000000L;
	private long maximumDelay = 64L;
	private long delay = 1L;

    private long maximumRetries = 5L;

	private B2ApiClient _client;

    public void setBucketId(String bucketId) {
        this.bucketId = bucketId;
    }

    public String getBucketId() {
        return bucketId;
    }

    public void setAccountId(String accountId) throws IllegalArgumentException {
        if (accountId.length() < 1)
            throw new IllegalArgumentException("Invalid accountId");
        this.accountId = accountId;
        _client = null;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setApplicationKey(String applicationKey) throws IllegalArgumentException {
        if (applicationKey.length() < 1)
            throw new IllegalArgumentException("Invalid applicationKey");
        this.applicationKey = applicationKey;
        _client = null;
    }

    public String getApplicationKey() { return applicationKey; }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPassword() { return password; }

    public void setAad(String aad) {
        this.aad = aad;
    }

    public String getAad() {
        return aad;
    }

    public long getMaximumRetries() {
        return maximumRetries;
    }

    public void setMaximumRetries(long maximumRetries) {
        if (maximumRetries>0) {
            this.maximumRetries = maximumRetries;
        }
    }

    public long getMaximumDelay() {
        return maximumDelay;
    }

    public void setMaximumDelay(long maximumDelay) {
        if (maximumDelay > 0) {
            this.maximumDelay = maximumDelay;
        }
    }

	public long getLargeFileLimit() {
		return largeFileLimit;
	}

	public void setLargeFileLimit(long largeFileLimit) {
		if (largeFileLimit > LARGE_FILE_LIMIT) {
			this.largeFileLimit = LARGE_FILE_LIMIT;
		}
		else if (largeFileLimit <= this.recommendedPartSize) {
			this.largeFileLimit = this.recommendedPartSize + 1;
		}
		else {
			this.largeFileLimit = largeFileLimit;
		}

		// Ensure the partSize is compatible with new maximum
		this.setPartSize(this.partSize);
	}

	public long getPartSize() {
		return partSize;
	}

	public void setPartSize(long partSize) {
		if (partSize >= this.largeFileLimit) {
			this.partSize = this.largeFileLimit - 1;
		}
		else if (partSize < this.recommendedPartSize) {
			this.partSize = this.recommendedPartSize;
		}
		else {
			this.partSize = partSize;
		}
	}

    private B2ApiClient getClient() throws IOException
    {
        if (_client == null) {
            if (accountId == null)
                throw new IOException("Account ID not set");
            else if (applicationKey == null)
                throw new IOException("Application Key not set");
            else if (bucketId == null)
                throw new IOException("Bucket ID not set");

            try {
                _client = new B2ApiClient(accountId, applicationKey);
            }
            catch (B2ApiException e) {
                throw new IOException("Unable to create client", e);
            }

            recommendedPartSize = _client.getRecommendedPartSize();

            // Reset the large file upload limits to ensure they comply with recommendedPartSize
            setLargeFileLimit(largeFileLimit);
            setPartSize(partSize);
        }
        return _client;
    }

	boolean verify(String fileId) throws IOException
	{
        B2ApiClient client = getClient();

		long count = 0;
        Exception err;
		while (true) {
            try {
                client.getFileInfo(fileId);
                return true;
            } catch (B2ApiException e) {
                // Handle the not_found errors, since we are checking for existence
                if (e.getStatus() == 404) {
                    // 404  not_found   file_state_deleted
                    // 404  not_found   file_state_none
                    // 404  not_found   file_state_unknown
                    return false;
                }
                // Process any other errors
                processException(e);
                err = e;
            }
            count++;
            if (count > getMaximumRetries()) {
                throw new IOException("Unable to get file info", err);
            }
        }
	}

	
	void download(File localFile, String fileId) throws IOException
	{
        B2ApiClient client = getClient();

        Exception err;
		FileOutputStream os = new FileOutputStream(localFile);
		InflaterOutputStream inflate = new InflaterOutputStream(os);
        String digest;
		try {
            int count = 0;
            while (true) {
                try {
                    B2DownloadFileResponse result = client.downloadFileById(fileId);

                    if (!MIME_TYPE.equals(result.getContentType())) {
                        throw new IOException("ContentType mismatch " + result.getContentType() + " != " + MIME_TYPE);
                    }

                    InputStream is = result.getContent();
                    try {
                        digest = AesGcm.decrypt(password, aad, is, inflate);
                    } catch (IOException e) {
                        throw new IOException("Failed to decrypt " + fileId);
                    } finally {
                        is.close();
                    }

                    if (!digest.equals(result.getContentSha1())) {
                        throw new IOException("SHA1 mismatch " + digest + " != " + result.getContentSha1());
                    }
                    break;
                } catch (B2ApiException e) {
                    processException(e);
                    err = e;
                }
                count++;
                if (count > getMaximumRetries()) {
                    throw new IOException("Unable to download file by id to stream", err);
                }
            }
        }
		finally {
            inflate.close();
            os.close();
        }
	}
	

	String upload(File localFile, String fileName) throws IOException
	{
		String fileId = null;

		String sha1 = null;
		Map<String, String> fileInfo = new HashMap<String, String>();
		fileInfo.put("src_last_modified_millis", Long.toString(localFile.lastModified()));

		File tempFile = File.createTempFile("b2-", ".upload", null);
		try {
            FileOutputStream tempOS = new FileOutputStream(tempFile);
            FileInputStream localIS = new FileInputStream(localFile);
            DeflaterInputStream deflator = new DeflaterInputStream(localIS);
            try {
                sha1 = AesGcm.encrypt(password, aad, deflator, tempOS);
            } finally {
                deflator.close();
                localIS.close();
                tempOS.close();
            }

            long fileSize = tempFile.length();

            if (fileSize > getLargeFileLimit()) {
                // Upload large file
                fileInfo.put("large_file_sha1", sha1);

                MessageDigest sha1Digest;
                try {
                    sha1Digest = MessageDigest.getInstance("SHA-1");
                } catch (NoSuchAlgorithmException e) {
                    throw new IOException("Hard coded digest method is wrong");
                }

                startLargeFileUpload(fileName, fileInfo);

                fileId = startLargeFileUpload(fileName, fileInfo);

                try {
                    // Calculate the parts for the file
                    long partSize = this.getPartSize();
                    long partTotal = fileSize / partSize;

                    // Add the extra partial part
                    if (partSize * partTotal < fileSize)
                        partTotal++;

                    // Allocate the array for the sha1 of each part
                    String[] partSha1Array = new String[(int) partTotal];

                    // Allocate the buffer for one part
                    byte[] buf = new byte[(int) partSize];

                    // Setup for reading the file
                    FileInputStream fileIS = new FileInputStream(tempFile);

                    B2GetUploadPartUrlResponse responseUrl = getUploadPartUrl(fileId);

                    for (int partNumber = 1; partNumber <= partTotal; partNumber++) {
                        // Read in the next part
                        int bytesForPart = fileIS.read(buf, 0, (int) partSize);

                        // Compute the SHA1 for the part
                        sha1Digest.reset();
                        sha1Digest.update(buf, 0, bytesForPart);
                        sha1 = String.format("%040x", new BigInteger(1, sha1Digest.digest()));
                        partSha1Array[partNumber - 1] = sha1;

                        responseUrl = uploadLargeFilePart(fileId, responseUrl, partNumber, new ByteArrayEntity(buf, 0, bytesForPart), sha1);
                    }
                    fileIS.close();

                    finishLargeFileUpload(fileId, partSha1Array);
                } catch (IOException e) {
                    // Unrecoverable error uploading parts, so cancel
                    cancelLargeFileUpload(fileId);
                    throw e;
                }
            } else {
                fileId = uploadFile(fileName, new FileEntity(tempFile), sha1, fileInfo);
            }
        }
		finally {
            // Delete the temporary file
            //noinspection ResultOfMethodCallIgnored
            tempFile.delete();
        }
		
		
		if (fileId == null)
			throw new IOException("Failed to upload file");
		
		return fileId;
	}


	private String resolveBucketId(String bucketName) throws IOException {
        B2ApiClient client = getClient();

        int count = 0;
        Exception err;
		while (true) {
			try {
                List<B2BucketResponse> buckets = client.listBuckets();
                this.delay = 1L;

				for (B2BucketResponse response : buckets) {
					if (bucketName.equals(response.getBucketName())) {
						return response.getBucketId();
					}
				}
				throw new IOException("No such bucket name");
			}
			catch (B2ApiException e) {
				processException(e);
                err = e;
			}
            count++;
            if (count > getMaximumRetries()) {
                throw new IOException("Unable to get bucket id", err);
            }
		}
	}

	private String startLargeFileUpload(String fileName, Map<String, String> fileInfo) throws IOException {
        B2ApiClient client = getClient();

        long count = 0;
        Exception err;
		while (true) {
            try {
                B2StartLargeFileResponse response = client.startLargeFileUpload(bucketId, fileName, MIME_TYPE, fileInfo);
                this.delay = 1L;
                return response.getFileId();
            } catch (B2ApiException e) {
                processException(e);
                err = e;
            }
            count++;
            if (count > getMaximumRetries()) {
                throw new IOException("Unable to start large file upload", err);
            }
        }
    }

    private void finishLargeFileUpload(String fileId, String [] partSha1Array) throws IOException {
        B2ApiClient client = getClient();

        long count = 0;
        Exception err;
		while (true) {
            try {
                client.finishLargeFileUpload(fileId, partSha1Array);
                this.delay = 1;
                return;
            } catch (B2ApiException e) {
                processException(e);
                err = e;
            }
            count++;
            if (count > getMaximumRetries()) {
                throw new IOException("Unable to finish large file upload", err);
            }
        }
    }

    private void cancelLargeFileUpload(String fileId) throws IOException {
        B2ApiClient client = getClient();

        long count = 0;
        Exception err;
        while (true) {
            try {
                client.cancelLargeFileUpload(fileId);
                this.delay = 1;
                return;
            }
            catch (B2ApiException e) {
                processException(e);
                err = e;
            }
            count++;
            if (count > getMaximumRetries()) {
                throw new IOException("Unable to cancel large file upload", err);
            }
        }
    }

    private B2GetUploadPartUrlResponse getUploadPartUrl(String fileId) throws IOException {
        B2ApiClient client = getClient();

        long count = 0;
        Exception err;
        while (true) {
            try {
                B2GetUploadPartUrlResponse responseUrl = client.getUploadPartUrl(fileId);
                this.delay = 1L;
                return responseUrl;
            } catch (B2ApiException e) {
                processException(e);
                err = e;
            }
            count++;
            if (count > getMaximumRetries()) {
                throw new IOException("Unable to obtain upload part url", err);
            }
        }
    }

    private B2GetUploadPartUrlResponse uploadLargeFilePart(String fileId, B2GetUploadPartUrlResponse responseUrl, int partNumber, ByteArrayEntity entity, String sha1) throws IOException {
        B2ApiClient client = getClient();

        long count = 0;
        Exception err;
        while (true) {
            try {
                // Try uploading
                client.uploadLargeFilePart(responseUrl, partNumber, entity, sha1);
                this.delay = 1L;
                return responseUrl;
            } catch (B2ApiException e) {
                processException(e);
                // Get new upload URL and authorization token after any failure
                responseUrl = getUploadPartUrl(fileId);
                err = e;
            } catch (SocketException e) {
                processSocket(e);
                responseUrl = getUploadPartUrl(fileId);
                err = e;
            } catch (SocketTimeoutException e) {
                processSocket(e);
                responseUrl = getUploadPartUrl(fileId);
                err = e;
            }
            count++;
            if (count > getMaximumRetries()) {
                throw new IOException("Unable to upload large file part", err);
            }
        }
    }

    private String uploadFile(String fileName, FileEntity data, String sha1, Map<String, String> fileInfo) throws IOException {
        B2ApiClient client = getClient();

        long count = 0;
        Exception err;
		while (true) {
            try {
                // Automatically gets new uploadUrl and authorizationToken on each call
                // This is desired behaviour for B2 integrations
                B2FileResponse response = client.uploadFile(bucketId, fileName, data, sha1, MIME_TYPE, fileInfo);
                this.delay = 1L;
                return response.getFileId();
            }
            catch (B2ApiException e) {
                processException(e);
                err = e;
            } catch (SocketException e) {
                processSocket(e);
                err = e;
            } catch (SocketTimeoutException e) {
                processSocket(e);
                err = e;
            }
            count++;
            if (count > getMaximumRetries()) {
                throw new IOException("Unable to upload file", err);
            }
        }
    }

    private void processSocket(Exception e) throws IOException {
        JSONObject obj = new JSONObject();
        obj.put("status", 503);
        obj.put("message", "SocketException");
        obj.put("code", "socket_exception");
        processException(new B2ApiException(obj.toString(), e));
    }

	private void processException(B2ApiException e) throws IOException
	{

		/*
		The HTTPS connection cannot be made. (e.g., connection timeout).
		The HTTPS connection is broken. (e.g., broken pipe.)
		The HTTPS connection times out waiting for a response (socket timeout).
		The HTTP response status is 408 (Request Timeout).
		The HTTP response status of 401 Unauthorized, and an error code of expired_auth_token
		The HTTP response status is between 500-599, including 503 Service Unavailable.

		Testing
		X-Bz-Test-Mode: fail_some_uploads
		 */

        boolean reauthorize = false;

		long sleep = e.getRetry() == null ? 0 : e.getRetry();

		// If a retry after was specified, then reset the exponential backup
		if (sleep > 0) {
			this.delay = 1;
		}

		String code = e.getCode();
		int status = e.getStatus();

		// Terminal issues that are not processed
		// 400 bad_request              Account <accountId> does not exist                                  bug in code
        // 400 bad_request              No Authorization header                                             bug in code
        // 401 unauthorized			    Not authorized                                                      accountId and/or app key are wrong
        //                                                                                                  bug in code
        // 403 cap_exceeded			    operations/capacity has been exceeded
        // 404 not_found                file_state_deleted                                                  requested deleted file
        // 404 not_found                file_state_none                                                     requested non-existing file
        // 404 not_found                file_state_unknown                                                  requested unknown file
        // 405 method_not_allowed	    bug in code
        // 416 range_not_satisfiable    The Range header in the request is outside the size of the file.

		if (status == 401) {
			if (code.equals("expired_auth_token") ||
					code.equals("missing_auth_token") ||
					code.equals("bad_auth_token"))
			{
                // Force reauthorization for any of these errors
				reauthorize = true;
				e = null;
			}
		}
		else if ((status == 429) ||
                ((status == 408) && (code.equals("request_timeout"))) ||
                ((status >= 500) && (status <= 599))) {
			// 429 Too many requests
            // 428 request_timeout
            // 503 service unavailable

            // sleep was not set by retry-after use exponential backup
            if (sleep <= 0) {
				sleep = this.delay;
				this.delay *= 2;
			}
			else {
                this.delay = 1L;
            }
			e = null;
		}

		if (sleep > 0)
		{
            if (this.delay > getMaximumDelay()) {
                throw new IOException("Timeout accessing B2");
            }

			try
			{
				Thread.sleep(sleep);
			}
			catch(InterruptedException err) {
				// Continue processing
			}
		}

		if (reauthorize) {
            B2ApiClient client = getClient();

			// Renew the auth token
			try {
				client.authenticate(accountId, applicationKey);
				this.delay = 1;
			} catch (B2ApiException err) {
				throw new IOException(err.getMessage(), err);
			}
		}

		// Rethrow the error, because it cannot be processed
		if (e == null) {
            String msg = e.getMessage();
            if (msg == null) {
                msg = "Unprocessed B2ApiException";
            }
			throw new IOException(msg, e);
		}
	}
}
